# Copyright 2020 Juan Pablo Samper
# SPDX-License-Identifier: Apache-2.0

from ade_cli.utils import get_docker_version


def test_version(mocker):
    mocker.patch('ade_cli.utils.runout', return_value='19.03.8')
    version = get_docker_version()
    assert version == (19, 3, 8)


def test_ce_version(mocker):
    mocker.patch('ade_cli.utils.runout', return_value='19.04.5-ce')
    version = get_docker_version()
    assert version == (19, 4, 5)
